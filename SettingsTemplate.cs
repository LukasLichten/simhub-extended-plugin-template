﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extended_Plugin_Template
{
    public class SettingsTemplate
    {
        public bool ExampleEnable { get; set; } = false;
        public int ExampleNumber { get; set; } = 5;
        public string ExampleText { get; set; } = "";
    }
}
