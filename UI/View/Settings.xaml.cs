﻿using Extended_Plugin_Template.UI.Model;
using System.Windows;
using System.Windows.Controls;

namespace Extended_Plugin_Template.UI.View
{
    /// <summary>
    /// A simple Settings panel, with some examples for handling booleans, text and numbers.
    /// And able to revert unsaved changes
    /// </summary>
    public partial class Settings : UserControl
    {
        private ExtendedPluginTemplate Main { get; set; }

        private SettingsModel Model { get; set; }

        public Settings()
        {
            InitializeComponent();
        }

        public Settings(ExtendedPluginTemplate main) : base()
        {
            // You can simplify this if you intend to just use this settings panel
            Init(main);
        }

        internal void Init(ExtendedPluginTemplate main)
        {
            Main = main;

            LoadData();
        }

        private void LoadData()
        {
            //Generating DataModel from the settings of Main
            Model = new SettingsModel()
            {
                ExampleEnable = Main.Settings.ExampleEnable,
                ExampleNumber = Main.Settings.ExampleNumber,
                ExampleText = Main.Settings.ExampleText,

                Changed = false, //Reset to disable the Apply/Revert buttons
            };

            DataContext = Model;
        }

        private void BtnApply_Click(object sender, RoutedEventArgs e)
        {
            Main.Settings.ExampleEnable = Model.ExampleEnable;
            Main.Settings.ExampleNumber = Model.ExampleNumber;
            Main.Settings.ExampleText = Model.ExampleText;



            Model.Changed = false; //Reset to disable the Apply/Revert buttons

            //If you need to update some property (like resizing an array, reconnect a web client, etc) you should call the function to do that here
        }

        private void BtnRevert_Click(object sender, RoutedEventArgs e)
        {
            LoadData();
        }
    }
}
