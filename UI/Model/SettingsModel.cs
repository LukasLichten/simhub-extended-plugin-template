﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Extended_Plugin_Template.UI.Model
{
    internal class SettingsModel :INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _changed;

        private bool _exampleEnable;

        private int _exampleNumber;

        private string _exampleText;


        public bool ExampleEnable
        {
            get => _exampleEnable;
            set
            {
                _exampleEnable = value;
                OnPropertyChanged();
                Changed = true;
            }
        }

        public int ExampleNumber
        {
            get => _exampleNumber;
            set
            {
                _exampleNumber = value;
                if (_exampleNumber < 1) // Insures there will be only positive numbers
                    _exampleNumber = 1;

                OnPropertyChanged();
                Changed = true;
            }
        }

        public string ExampleText
        {
            get => _exampleText;
            set
            {
                _exampleText = value;
                OnPropertyChanged();
                Changed = true;
            }
        }

        public bool Changed
        {
            get => _changed;
            set
            {
                _changed = value;
                OnPropertyChanged();
            }
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
