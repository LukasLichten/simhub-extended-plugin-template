﻿using GameReaderCommon;
using SimHub.Plugins;
using System;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Collections.Generic;
using Lichten.SimHubHelper;
using System.Windows.Media;
using Extended_Plugin_Template.UI.View;

namespace Extended_Plugin_Template
{
    // Change the names in the class attributes, don't forget to rename the class and change LeftMenuTitle as well.
    // Also, when changing the namespace use rename tool, it will fix it also for the UI and Settings Container.
    // For future classes change it in the Project Properties
    // Important: The namespace has to have a different name then this class, else the xaml for the UI will not compile
    // There also change the Assembly name and don't forget the Assembly Information
    
    // The Output Path for Debug is set to point back to SimHub install when: 
    // -SimHub is on the same drive as the project
    // -The project sits two folders off the root of that drive
    //      Example: c:/git/ExtendedPluginTemplate/
    // Debugger assumes you are on the C drive, you have to maybe point this to another drive manually

    [PluginName("Extended Plugin Template")]
    [PluginAuthor("Name")]
    [PluginDescription("Describe Me!")]
    public class ExtendedPluginTemplate : IPlugin, IDataPlugin, IWPFSettingsV2 //IOutputPlugin //output plugin can be used instead of IDataPlugin, will result in DataUpdate Function being run later
    {
        public SettingsTemplate Settings { get; set; }

        /// <summary>
        /// Instance of the current plugin manager
        /// </summary>
        public PluginManager PluginManager { get; set; }

        /// <summary>
        /// Gets the left menu icon. Icon must be 24x24 and compatible with black and white display.
        /// </summary>
        public ImageSource PictureIcon => this.ToIcon(Properties.Resources.sdkmenuicon);

        /// <summary>
        /// Gets a short plugin title to show in left menu. Return null if you want to use the title as defined in PluginName attribute.
        /// </summary>
        public string LeftMenuTitle => "Extended Plugin Template";

        /// <summary>
        /// Called after plugins startup
        /// </summary>
        /// <param name="pluginManager"></param>
        public void Init(PluginManager pluginManager)
        {
            PluginManager = pluginManager;

            // Load settings
            Settings = this.ReadCommonSettings<SettingsTemplate>("ExtendedPluginTemplate", () => new SettingsTemplate());

            pluginManager.NewLap += new PluginManager.NewLapDelegate(NewLapUpdate);

            //Example
            pluginManager.AddProperty("ExampleTime", this.GetType(), TimeSpan.Zero.GetType(), "Optional Description");
            pluginManager.AddProperty("A_Time", this.GetType(), double.MaxValue.GetType());
        }

        /// <summary>
        /// called one time per game data update
        /// </summary>
        /// <param name="pluginManager"></param>
        /// <param name="data"></param>
        public void DataUpdate(PluginManager pluginManager, ref GameData data)
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            //Insert here properties
            pluginManager.SetPropertyValue("ExampleTime", this.GetType(), TimeSpan.Zero);

            stopwatch.Stop();
            pluginManager.SetPropertyValue("A_Time", this.GetType(), stopwatch.Elapsed.TotalMilliseconds); //Logging the execution time of your plugin can be useful for optimization
        }



        /// <summary>
        /// Registered on init, will trigger when a lap is completed
        /// You can remove it's registration from init and then you can remove the function
        /// </summary>
        /// <param name="completedLapNumber">This </param>
        /// <param name="testLap"></param>
        /// <param name="manager"></param>
        /// <param name="data"></param>
        public void NewLapUpdate(int completedLapNumber, bool testLap, PluginManager manager, ref GameData data)
        {
            // data.NewData.CompletedLaps == completedLapNumber + 1;
        }

        /// <summary>
        /// Called at plugin manager stop, close/displose anything needed here !
        /// </summary>
        /// <param name="pluginManager"></param>
        public void End(PluginManager pluginManager)
        {
            this.SaveCommonSettings<SettingsTemplate>("ExtendedPluginTemplate", Settings);
        }

        /// <summary>
        /// Used to gather 
        /// </summary>
        /// <param name="pluginManager"></param>
        /// <returns></returns>
        public System.Windows.Controls.Control GetWPFSettingsControl(PluginManager pluginManager)
        {
            // This is a multitab control panel
            return new MainControl(this);

            // This is a simple settings panel
            //return new Settings(this);
        }


    }
}