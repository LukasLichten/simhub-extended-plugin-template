﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lichten.SimHubHelper
{
    /// <summary>
    /// Used to iterate up and down a defined list.
    /// The setter function that is triggered for the action has to take string as input.
    /// The ListHelper is used to get the current value and all possible values
    /// 
    /// HandleSwitch can be overwritten to allow for cases where you are not dealing with a string list
    /// </summary>
    public class ListActionHelper
    {
        public Action<string> Method { get; set; }
        public ListHelper Helper { get; set; }

        public ListActionHelper() { }

        public ListActionHelper(Action<string> method, ListHelper helper) : this()
        {
            Method = method;
            Helper = helper;
        }

        protected virtual void HandleSwitch(int offsetIndex)
        {
            string[] list = Helper.GetList();

            string current = Helper.GetCurrent();
            int index = -1;

            //Finding the current items index
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i] == current)
                {
                    index = i;
                    break;
                }
            }

            if (index == -1)
                throw new Exception("Item was not found to be in the list that it should be in");

            int targetIndex = index + offsetIndex;

            //This makes it loop around when either end is reached
            while (targetIndex >= list.Length) //there should be only one loop of these at max, but you never know if offset is 10x
            {
                offsetIndex = targetIndex - list.Length;
                targetIndex = 0 + offsetIndex;
            }
            while (targetIndex < 0)
            {
                offsetIndex = targetIndex;
                targetIndex = list.Length + offsetIndex;
            }

            //Getting the target and setting it
            string target = list[targetIndex];
            Method.Invoke(target);
            //Method.BeginInvoke(target, null, null);
        }

        public void TriggerNextAction(PluginManager pluginManager, string data)
        {
            try
            {
                Action<int> act = HandleSwitch;
                act.BeginInvoke(1, null, null);
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to execute Action " + data + ": " + ex.Message);
            }
        }

        public void TriggerPreviousAction(PluginManager pluginManager, string data)
        {
            try
            {
                Action<int> act = HandleSwitch;
                act.BeginInvoke(-1, null, null);
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to execute Action " + data + ": " + ex.Message);
            }
        }
    }
}
