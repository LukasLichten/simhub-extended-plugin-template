﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lichten.SimHubHelper
{
    /// <summary>
    /// This is to simply hook up an EventHandler, which when triggered fires the SimHub Event
    /// You need to call the RegisterEvent function, otherwise you can not trigger the event
    /// </summary>
    public class EventHelper
    {
        public string EventName { get; set; }
        public Type Plugin { get; set; }
        public PluginManager PluginManager { get; set; }

        protected EventHelper() { }
        public EventHelper(string eventName, Type plugin, PluginManager pluginManager)
        {
            EventName = eventName;
            Plugin = plugin;
            PluginManager = PluginManager;
        }

        /// <summary>
        /// This is required to be called, otherwise the event can not be triggered
        /// It returns this EventHelper, you don't need to update your reference, but this is useful to chain EventHandle onto it
        /// 
        /// This is in a seperate function to allow you to create seperate Events coming from the same EventHandle
        /// </summary>
        /// <returns></returns>
        public EventHelper RegisterEvent()
        {
            PluginManager.AddEvent(EventName, Plugin);
            return this;
        }

        protected void TriggerEvent(string name)
        {
            try
            {
                PluginManager.TriggerEvent(name, Plugin);
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("Exception thrown when trying to execute Event " + Plugin.Name + "." + name + ": " + ex.Message);
            }
        }

        public void EventHandle(object sender, EventArgs e)
        {
            TriggerEvent(EventName);
        }
    }
}
